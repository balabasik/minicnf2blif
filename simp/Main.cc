/*****************************************************************************************[Main.cc]
Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson
Copyright (c) 2007-2010, Niklas Sorensson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************************************/

#include <errno.h>

#include <signal.h>
#include <zlib.h>

#include "utils/System.h"
#include "utils/ParseUtils.h"
#include "utils/Options.h"
#include "core/Dimacs.h"
#include "simp/SimpSolver.h"

using namespace Minisat;

//=================================================================================================


void printStats(Solver& solver)
{
/*
    double cpu_time = cpuTime();
    double mem_used = memUsedPeak();
    printf("restarts              : %"PRIu64"\n", solver.starts);
    printf("conflicts             : %-12"PRIu64"   (%.0f /sec)\n", solver.conflicts   , solver.conflicts   /cpu_time);
    printf("decisions             : %-12"PRIu64"   (%4.2f %% random) (%.0f /sec)\n", solver.decisions, (float)solver.rnd_decisions*100 / (float)solver.decisions, solver.decisions   /cpu_time);
    printf("propagations          : %-12"PRIu64"   (%.0f /sec)\n", solver.propagations, solver.propagations/cpu_time);
    printf("conflict literals     : %-12"PRIu64"   (%4.2f %% deleted)\n", solver.tot_literals, (solver.max_literals - solver.tot_literals)*100 / (double)solver.max_literals);
    if (mem_used != 0) printf("Memory used           : %.2f MB\n", mem_used);
    printf("CPU time              : %g s\n", cpu_time);
*/
}


//static Solver* solver;
// Terminate by notifying the solver and back out gracefully. This is mainly to have a test-case
// for this feature of the Solver as it may take longer than an immediate call to '_exit()'.
//static void SIGINT_interrupt(int signum) { solver->interrupt(); }

// Note that '_exit()' rather than 'exit()' has to be used. The reason is that 'exit()' calls
// destructors and may cause deadlocks if a malloc/free function happens to be running (these
// functions are guarded by locks for multithreaded use).
/*static void SIGINT_exit(int signum) {
    printf("\n"); printf("*** INTERRUPTED ***\n");
    if (solver->verbosity > 0){
        printStats(*solver);
        printf("\n"); printf("*** INTERRUPTED ***\n"); }
    _exit(1); }*/
    
static int ndigits(int number) {
int answer=0;
if (number==0) {return 1;}
while (number>0) {number/=10;answer++;}
return answer;
}

//=================================================================================================
// Sorting int vector according to weights from another vector
struct intpair {int value; int weight;};
struct myveclt {bool operator() (intpair a, intpair b) {return a.weight < b.weight;}};

void WeightedSort(vec<int>& array, const vec<int>& weights) {

vec<int> copyarray;
vec<intpair> order;
intpair pair1;
myveclt lessthan;

for (int i=0; i<array.size(); i++) {pair1.value=i;pair1.weight=weights[i];order.push(pair1);}
sort(order,lessthan);

for (int i=0;i<order.size();i++) {copyarray.push(array[order[i].value]);}
copyarray.moveTo(array);

return;
}


//=================================================================================================
// Main:


int main(int argc, char** argv)
{
    try {
        setUsageHelp("USAGE: %s [options] <input-file> <result-output-file>\n\n  where input may be either in plain or gzipped DIMACS.\n");
        // printf("This is MiniSat 2.0 beta\n");
        
/*#if defined(__linux__)
        fpu_control_t oldcw, newcw;
        _FPU_GETCW(oldcw); newcw = (oldcw & ~_FPU_EXTENDED) | _FPU_DOUBLE; _FPU_SETCW(newcw);
        printf("WARNING: for repeatability, setting FPU to use double precision\n");
#endif*/
        // Extra options:
        //
        IntOption    verb        ("MAIN", "verb",   "Verbosity level (0=silent, 1=some, 2=more).", 0, IntRange(0, 2));
        IntOption    cpu_lim     ("MAIN", "cpu-lim","Limit on CPU time allowed in seconds.\n", INT32_MAX, IntRange(0, INT32_MAX));
        IntOption    mem_lim     ("MAIN", "mem-lim","Limit on memory usage in megabytes.\n", INT32_MAX, IntRange(0, INT32_MAX));
        BoolOption   writedep    ("MAIN", "writedep", "If given write variable dependencies into .dep file", false);
        BoolOption   disactivate ("MAIN", "disactivate", "Disactivate clauses participating in already found definitions", true);
		BoolOption   qbfmode     ("MAIN", "qbfmode", "To use qbf solver instead of GMUS", false);
		BoolOption   pre         ("MAIN", "pre", "To turn preprocessing on/off", false);

        
        parseOptions(argc, argv, true);
        double initial_time = cpuTime();
        
        printf("\n================================================\n");
        printf(" STARTING the program.\n");
        
        // Set limit on CPU-time:
        if (cpu_lim != INT32_MAX){
            rlimit rl;
            getrlimit(RLIMIT_CPU, &rl);
            if (rl.rlim_max == RLIM_INFINITY || (rlim_t)cpu_lim < rl.rlim_max){
                rl.rlim_cur = cpu_lim;
                if (setrlimit(RLIMIT_CPU, &rl) == -1)
                    printf("WARNING! Could not set resource limit: CPU-time.\n");
            } }

        // Set limit on virtual memory:
        if (mem_lim != INT32_MAX){
            rlim_t new_mem_lim = (rlim_t)mem_lim * 1024*1024;
            rlimit rl;
            getrlimit(RLIMIT_AS, &rl);
            if (rl.rlim_max == RLIM_INFINITY || new_mem_lim < rl.rlim_max){
                rl.rlim_cur = new_mem_lim;
                if (setrlimit(RLIMIT_AS, &rl) == -1)
                    printf("WARNING! Could not set resource limit: Virtual memory.\n");
            } }
        
        if (argc == 1)
            printf("Reading from standard input... Use '--help' for help.\n");
        
        gzFile in = (argc == 1) ? gzdopen(0, "rb") : gzopen(argv[1], "rb");
        if (in == NULL)
            printf("ERROR! Could not open file: %s\n", argc == 1 ? "<stdin>" : argv[1]), exit(1);
        
        int vars, clauses;
        bool qbf;
        vec<int> prefix; 	     //QBF prefix; also will be used as activation of variables
        vec<int> qlevel;         //Quantification level of each variable
        vec<int> matrix, breaks; //2dim array containing CNF
        vec<int> poccs, pbreaks; //2dim array containing positive occurrences
        vec<int> noccs, nbreaks; //2dim array containing negative occurrences 
        
        parse_DIMACS(in, matrix, breaks, prefix, qlevel, qbf);
        gzclose(in);
        
        clauses = breaks.size() - 1;
        vars = prefix.size();
        vec<bool> active_clauses, syntactic;
        //vec<int> var_status;
        if (disactivate) for (int i=0;i<clauses;i++) {active_clauses.push(true);}
        //for (int i=0;i<vars;i++) {var_status.push(0);}
        buildOccLists(matrix, breaks, poccs, pbreaks, noccs, nbreaks, vars);
        
        double parsetime = cpuTime(); 
        printf(" PARSING done. Time: %f\n", parsetime-initial_time);
        
        // Change to signal-handlers that will only notify the solver and allow it to terminate
        // voluntarily:
        //signal(SIGINT, SIGINT_interrupt);
        //signal(SIGXCPU,SIGINT_interrupt);
        
        lbool ret;
        vec<int> pclauses, nclauses;
        int ngood = 0, syngood = 0, nbad = 0, nzombie = 0, nskipped = 0;
        double qbftime = 0;
        double pretime = 0;
        double gettime = 0;
        double sattime = 0;
        double unsattime = 0;
        char * filename = argv[1];
        
        int maxclauses = 200;
        int conflimit = 10;
        
        strcat(filename,".dep");
        FILE* res = writedep ? fopen(filename, "w") : NULL;
        
        if (res) 
        	{
        	if (qbf) {fprintf(res, "p qbf %d\n",vars);}
        	else {fprintf(res, "p cnf %d\n",vars);}
        	}
        
        vec<int> order;
        for (int i=0;i<vars;i++) {order.push(i);syntactic.push(false);} 
        WeightedSort(order,prefix);
        
        vec<int> binarylits, binarymap;
        vec<int> subclause, subset_indexes, clause_indexes;
        
        for (int i=0 ; i < 2*vars ; i++)
        	{
        	int var = order[i/2]; 
        	if (syntactic[var] || prefix[var]<=0) {continue;}
        	
        	vec<int>* occlist  = (i%2==0 ? &poccs : &noccs);
        	vec<int>* nocclist = (i%2!=0 ? &poccs : &noccs);
        	int pr0 = (i%2==0 ? pbreaks[var] : nbreaks[var]);
        	int pr1 = (i%2==0 ? pbreaks[var+1] : nbreaks[var+1]);
        	int nr0 = (i%2!=0 ? pbreaks[var] : nbreaks[var]);
        	int nr1 = (i%2!=0 ? pbreaks[var+1] : nbreaks[var+1]);
        	
        	int cstart, cend;
        	
        	//look for positive definitions
        	binarylits.clear();
        	clause_indexes.clear();
        	for (int j=pr0;j<pr1;j++)
        		{
        		//NOTE: this step is good for AND/OR gate detection speedup!!
        		//if (disactivate && !active_clauses[(*occlist)[j]]) {continue;}
        		
        		cstart = breaks[(*occlist)[j]];
        		cend = breaks[(*occlist)[j]+1];
        		//UNIT clause
        		if (cend-cstart==1)
        			{
        			syngood++;
        			syntactic[var] = true;
        			if (verb>0)
        				{
        				printf(" - %d : SYNGOOD! [total: 1/%d]", var+1, pbreaks[var+1] - pbreaks[var] + nbreaks[var+1] - nbreaks[var]);
        				printf("\n");
        				}
        			if (res) {fprintf(res,"%d -1 0\n",var+1);}
        			break;
        			}
        		if (cend-cstart!=2) {continue;}
        		//NOTE: pushing negation of a literal for subset check
        		binarylits.push( abs(matrix[cstart])==(var+1) ? -matrix[cstart+1] : -matrix[cstart] );
        		if (disactivate) {clause_indexes.push((*occlist)[j]);}
        		}
        	if (binarylits.size()!=0)
        		{
        		binarymap.clear();
        		for (int j=0;j<binarylits.size();j++) {binarymap.push(j);}
        		WeightedSort(binarymap,binarylits);
        		
        		//sort(binarylits);
        		//printf("%d ---------------\n", var+1);
        		//for (int j=0;j<binarylits.size();j++) {printf("%d ",binarylits[j]);}
        		//printf("0\n");
        		//printf("-------------------\n");
        		for (int j=nr0;j<nr1;j++)
        			{
        			subclause.clear();
        			cstart = breaks[(*nocclist)[j]];
        			cend = breaks[(*nocclist)[j]+1];
        			for (int k=cstart;k<cend;k++) 
        				{
        				if (abs(matrix[k])!=var+1) {subclause.push(matrix[k]);}
        				}
        			sort(subclause);
        			//printf("+++++++\n");
        			//for (int j=0;j<subclause.size();j++) {printf("%d ",subclause[j]);}
        			//printf("0\n");
        			//printf("+++++++\n");
        			if (VecIncludes(binarylits,binarymap,subclause,subset_indexes))
        				{
        				syngood++;		
        				syntactic[var] = true;
        				
        				if (verb>0)
        					{
        					printf(" - %d : SYNGOOD! [total: %d/%d]", var+1, subclause.size(), pbreaks[var+1] - pbreaks[var] + nbreaks[var+1] - nbreaks[var]);
        					printf("\n");
        					}
        				if (res) 
        					{
        					fprintf(res,"%d ",var+1);
        					if (subclause.size()==0) {fprintf(res,"-1 ");}
        					else {for (int k=0;k<subclause.size();k++) {fprintf(res, "%d ",abs(subclause[k]));}}
        					fprintf(res,"0\n");
        					}
        					
        				//disactivating clauses participating in the definition
        				if (disactivate)
        					{
        					for (int k=0;k<subset_indexes.size();k++) 
        						{
        						//assert(active_clauses[clause_indexes[subset_indexes[k]]]);
        						active_clauses[clause_indexes[subset_indexes[k]]]=false;
        						}	
        					}
        				break;
        				}
        			}
        		}
        	}
        
        double syntime = cpuTime();  
        printf(" SYNTACTIC check done. Found definitions: %d Time: %f\n", syngood, syntime-parsetime);
            
        for (int i=0 ; i < vars ; i++)
        	{
        	//int var = order[vars - i - 1];
        	int var = order[i]; 
        	
        	if (verb>0) printf(" v %d : ",var+1);
        	//if (res) {fprintf(res,"%d [ %d ] ",var+1,prefix[var]);}
        	
        	if (syntactic[var])
        		{
        		if (verb>0) printf(" SYNTACTIC! \n");
        		continue;
        		}
        	
        	if (prefix[var]<=0) 
        		{
        		if (verb>0) printf(" zombie or universal \n");
        		//if (res) {fprintf(res,"0\n");}
        		nskipped++;
        		continue;
        		}
        
        	pclauses.clear();
        	nclauses.clear();
        	for (int j=pbreaks[var];j<pbreaks[var+1];j++) 
        		{
        		if (!disactivate || active_clauses[poccs[j]]) {pclauses.push(poccs[j]);}
        		}
        	for (int j=nbreaks[var];j<nbreaks[var+1];j++) 
        		{
        		if (!disactivate || active_clauses[noccs[j]]) {nclauses.push(noccs[j]);}
        		}
        		
        	if (pclauses.size()+nclauses.size() > maxclauses && maxclauses!=-1) 
        		{
        		if (verb>0) printf(" - SKIP! [total: %d]\n",pclauses.size()+nclauses.size());
        		//if (res) {fprintf(res,"0\n");}
        		nskipped++;
        		continue;
        		}
        	
        	SimpSolver qbfSolver;
        	if (!pre) {qbfSolver.eliminate(true);}
        	if (conflimit!=-1) {qbfSolver.setConfBudget(conflimit);}
        	
        	gettime -= cpuTime();
        	if (qbfmode) {getQbf(qbfSolver, matrix, breaks, var, vars, pclauses, nclauses);}
        	else {getGcnf(qbfSolver, matrix, breaks, var, vars, pclauses, nclauses);}
        	gettime += cpuTime();
        	
        	if (pre) 
        		{
        		pretime -= cpuTime();
        		for (int i=0;i<qbfSolver.lastx;i++) {qbfSolver.setFrozen(i,true);}
        		qbfSolver.eliminate(true);
        		pretime += cpuTime();
        		}
			
			if (!qbfSolver.okay()) 
				{
				ngood++;
				if (verb>0) printf(" - GOOD! solved by preprocessing!!\n");
				continue;
				}
			if (qbfSolver.nClauses()==0 && pre) 
				{
				nbad++;
				if (verb>0) printf(" - BAD! solved by preprocessing!!\n");
				continue;
				}


        	ret = l_False;
        	int innercounter = 0;
        	vec<bool> blocked;
        	vec<int> depon_vars, depon_cls;
        	int nPossibleDefs = 0;
        	bool outofscope = false;
        	
        	blocked.clear();
        	
        	while (ret == l_False)
        		{		
        		qbftime=cpuTime();
        		if (qbfmode) {ret = qbfSolver.solve_qbf(blocked);}
        		else {ret = qbfSolver.solve_gmus(blocked);}
        		qbftime=cpuTime()-qbftime;
        		
        		outofscope = false;
        		
        		if (ret == l_True && innercounter==0 ) 
        			{
        			nbad++;
        			unsattime+=qbftime;
        			if (verb>0) printf(" - BAD!  [total: %d]\n",pclauses.size()+nclauses.size());
        			//if (res) {fprintf(res,"0\n");}
        			}
        		else if (ret == l_Undef)
        			{
        			nbad++;
        			unsattime+=qbftime;
        			if (verb>0) printf(" - SKIP! [total: %d]\n",pclauses.size()+nclauses.size());
        			//if (res) {fprintf(res,"0\n");}
        			}
        		else if (ret == l_True)
        			{
        			unsattime+=qbftime;
        			}
        		else if (ret == l_False)
        			{
        			int ndefcls = 0;
        			nPossibleDefs++;
        			sattime+=qbftime;
        			
        			if (innercounter++ > 0) 
        				{
        				if (verb>0) 
        					{
        					printf("      ");
        					for (int j=0;j<ndigits(var+1);j++) {printf(" ");}
        					}
        				}
        			if (qbfSolver.qbfmodel.size() == 0) 
        				{
        				if (verb>0) printf(" zombie  [total: 0]\n");
        				//if (res) {fprintf(res,"0\n");}
        				nzombie++;
        				break;
        				}
        			if (verb>0) printf(" + GOOD! ");
					depon_vars.clear();
					depon_cls.clear();
        			for (int j=0;j<pclauses.size()+nclauses.size();j++)
        				{
        				if (!qbfSolver.qbfmodel[j]) 
        					{
        					ndefcls++;
        					if (j<pclauses.size()) 
        						{
        						//if (disactivate) depon_cls.push(1+pclauses[j]);
        						for (int k=breaks[pclauses[j]];k<breaks[pclauses[j]+1];k++)
        							{
        							bool found = false;
        							for (int l=0;l<depon_vars.size();l++)
        								{
        								if (abs(matrix[k])==depon_vars[l]) {found=true;break;}
        								}
        							if (!found && abs(matrix[k])!=var+1) 
        								{
        								depon_vars.push(abs(matrix[k]));
        								if (qlevel[var]<qlevel[abs(matrix[k])-1]) {outofscope=true;break;}
        								}	
        							}
        						if (outofscope) {break;}
        						}
        					else 
        						{
        						//if (disactivate) depon_cls.push(1+nclauses[j-pclauses.size()]);
        						for (int k=breaks[nclauses[j-pclauses.size()]];k<breaks[nclauses[j-pclauses.size()]+1];k++)
        							{
        							bool found = false;
        							for (int l=0;l<depon_vars.size();l++)
        								{
        								if (abs(matrix[k])==depon_vars[l]) {found=true;break;}
        								}
        							if (!found && abs(matrix[k])!=var+1) 
        								{
        								depon_vars.push(abs(matrix[k]));
        								if (qlevel[var]<qlevel[abs(matrix[k])-1]) {outofscope=true;break;}
        								}	
        							}
        						if (outofscope) {break;}
        						}
        					}
        				}
        			if (outofscope) 
        				{
        				if (verb>0) {printf("OUT_OF_SCOPE\n");} 
        				continue;
        				}
        			ngood++;
        			if (verb>0)
        				{
        				//if (ndefcls==0) {printf("constant");}
        				//else {for (int k=0;k<depon_vars.size();k++) {printf("%d ",depon_vars[k]);}}
        				printf("[total: %d/%d]", ndefcls, pclauses.size()+nclauses.size());
        				printf("\n");
        				}
        			if (res) 
        				{
        				fprintf(res,"%d ",var+1);
        				if (depon_vars.size()==0) {fprintf(res,"-1 ");}
        				else {for (int j=0;j<depon_vars.size();j++) {fprintf(res, "%d ",depon_vars[j]);}}
        				fprintf(res,"0\n");
        				}
        			}
        		//if (nPossibleDefs>=2) {break;}
        		break;
        		}
        	//if (nPossibleDefs == 0) {var_status[var]=-1;}	
        	//if (nPossibleDefs>0 && !outofscope) {ngood++;}
        	//note: it seems that this disactivation step ruins many definitions!
        	/*if (nPossibleDefs==1 && disactivate)
        		{
        		//var_status[var] = 1; // assigned
        		for (int j=0;j<depon_cls.size();j++) 
        			{
        			assert(active_clauses[depon_cls[j]-1]);
        			active_clauses[depon_cls[j]-1]=false;
        			}
        		}*/
        	} 
        if (res) {fprintf(res,"0\n");}  
        
        printf(" SEMANTIC  check done. Found definitions: %d Sem time: %f\n", ngood, cpuTime()-syntime);
        
        printf(" FINISHED. [ Syn/Sem/All: %d / %d / %d | get/pre/sat/uns: %f / %f / %f / %f ]\n", syngood, ngood, vars, gettime, pretime, sattime, unsattime); 
        printf(" Execution time: %f\n", cpuTime()-initial_time);
        printf("================================================\n\n");              
		return 0;	
    	} 
    catch (OutOfMemoryException&)
    	{
        printf("===============================================================================\n");
        printf("INDETERMINATE\n");
        exit(0);
    	}
}
