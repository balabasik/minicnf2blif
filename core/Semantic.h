#ifndef Minisat_Semantic_h
#define Minisat_Semantic_h

#include "core/Solver.h"
#include "core/Matrix.h"
#include "mtl/Vec.h"
#include <map>
#include <vector>

using std::vector;
using std::map;
using namespace Minisat;

//static Solver* solver;
// Terminate by notifying the solver and back out gracefully. This is mainly to have a test-case
// for this feature of the Solver as it may take longer than an immediate call to '_exit()'.
//static void SIGINT_interrupt(int signum) { solver->interrupt(); }

// Note that '_exit()' rather than 'exit()' has to be used. The reason is that 'exit()' calls
// destructors and may cause deadlocks if a malloc/free function happens to be running (these
// functions are guarded by locks for multithreaded use).
/*static void SIGINT_exit(int signum) {
    printf("\n"); printf("*** INTERRUPTED ***\n");
    if (solver->verbosity > 0){
        printStats(*solver);
        printf("\n"); printf("*** INTERRUPTED ***\n"); }
    _exit(1); }*/
    
    
 // Change to signal-handlers that will only notify the solver and allow it to terminate
        // voluntarily:
        //signal(SIGINT, SIGINT_interrupt);
        //signal(SIGXCPU,SIGINT_interrupt);

//=================================================================================================
/*
For semantic gates we specify defining clauses both for positive and negative 
polarities by allowing signed indexes.
Example: 1 -2 3 -4 5 means that positive phase clauses are 1 3 5, and negative -2 -4.
*/
        
void FindSemanticGates (Matrix& qbf, vector<int>& syntactic, vector<bool>& active_clauses, vector<Gate>& gates)
	{       
	Gate gate;
	gate.type = 3;
	gate.status = false;
	  
    int maxclauses = 200; //can set it to a higher value or -1 to turn off
    int conflimit = 10;	  

    lbool ret;
    vector<int> pclauses, nclauses;
    int ngood = 0, nbad = 0, nzombie = 0, nskipped = 0;
    
    vector<int> order;      
    for (int i=0;i<qbf.nvars;i++) {order.push_back(i);} 
    WeightedSort(order,qbf.prefix);
    			
	vector<int> semgate, posdefcls, negdefcls;  			
	map<int,int> depon_vars;
            
    for (int i = 0 ; i < qbf.nvars ; i++)
        {
        // note: can try different ordering here 
        // int var = order[qbf.nvars - i - 1];
        int var = order[i]; 
        	
        if (qbf.verb>0) printf(" v %d : ",var+1);
        
        // NOTE: we disable semantic check only if more than one syntactic definition was found for the given var
        // FIXME: since we only consider 1 definition now, we use value 0 for now (but generally should put it 1) 
        if (syntactic[var]>0)
        	{
        	if (qbf.verb>0) printf(" SYNTACTIC! \n");
        	continue;
        	}
        	
        if (qbf.prefix[var]<=0) 
        	{
        	if (qbf.verb>0) printf(" zombie or universal \n");
        	nskipped++;
        	continue;
        	}
        
        pclauses.clear();
        nclauses.clear();
        for (unsigned j=0;j<qbf.poccs[var].size();j++) 
        	{
        	if (active_clauses[qbf.poccs[var][j]]) {pclauses.push_back(qbf.poccs[var][j]);}
        	}
        for (unsigned j=0;j<qbf.noccs[var].size();j++) 
        	{
        	if (active_clauses[qbf.noccs[var][j]]) {nclauses.push_back(qbf.noccs[var][j]);}
        	}
        	
        if ((int)pclauses.size()+(int)nclauses.size() > maxclauses && maxclauses!=-1) 
        	{
        	if (qbf.verb>0) printf(" - SKIP! [total: %lu]\n",pclauses.size()+nclauses.size());
        	nskipped++;
        	continue;
        	}
        	
    	Solver qbfSolver;
       	if (conflimit!=-1) {qbfSolver.setConfBudget(conflimit);}

        qbfSolver.getGcnf(qbf.matrix, var, qbf.nvars, pclauses, nclauses);

       	ret = l_False;
        int innercounter = 0;
        vec<bool> blocked;
        int nPossibleDefs = 0;
        bool outofscope = false;
        	
        blocked.clear();
        	
        while (ret == l_False)
        	{
        	ret = qbfSolver.solve_gmus(blocked);
        	
        	outofscope = false;
        	
        	if (ret == l_True && innercounter==0 ) 
        		{
        		nbad++;
        		if (qbf.verb>0) printf(" - BAD!  [total: %lu]\n",pclauses.size()+nclauses.size());
        		}
    		else if (ret == l_Undef)
       			{
       			nbad++;
       			if (qbf.verb>0) printf(" - SKIP! [total: %lu]\n",pclauses.size()+nclauses.size());
       			}
        	else if (ret == l_False)
        		{
        		int ndefcls = 0;
        		nPossibleDefs++;
        		
        		if (innercounter++ > 0) 
        			{
        			if (qbf.verb>0) 
        				{
        				printf("      ");
        				for (int j=0;j<ndigits(var+1);j++) {printf(" ");}
        				}
        			}
        		if (qbfSolver.qbfmodel.size() == 0) 
        			{
        			if (qbf.verb>0) printf(" zombie  [total: 0]\n");
        			nzombie++;
        			break;
        			}
        			
        		if (qbf.verb>0) printf(" + GOOD! %d ", nPossibleDefs);
        		
				posdefcls.clear();
				negdefcls.clear();
				depon_vars.clear();
				
        		for (unsigned j=0;j<pclauses.size()+nclauses.size();j++)
        			{
        			if (!qbfSolver.qbfmodel[j]) 
        				{
        				ndefcls++;
        				if (j<pclauses.size()) 
        					{
        					posdefcls.push_back(pclauses[j]);
        					for (unsigned k=0;k<qbf.matrix[pclauses[j]].size();k++)
        						{
        						if (abs(qbf.matrix[pclauses[j]][k])==var+1) {continue;}
        						if (qbf.qlevel[var]<qbf.qlevel[abs(qbf.matrix[pclauses[j]][k])-1]) {outofscope=true;break;}
        						
        						// FIXME: this is cheating!!!
        						//if (qbf.prefix[var]<qbf.prefix[abs(qbf.matrix[pclauses[j]][k])-1]) {outofscope=true;break;}
        						
        						if (depon_vars.find(abs(qbf.matrix[pclauses[j]][k]))!=depon_vars.end()) {continue;}
								depon_vars.insert(std::pair<int,int>(abs(qbf.matrix[pclauses[j]][k]),0));			
        						}
        					if (outofscope) {break;}
        					}
        				else 
        					{
        					negdefcls.push_back(nclauses[j-pclauses.size()]);
        					for (unsigned k=0;k<qbf.matrix[nclauses[j-pclauses.size()]].size();k++)
        						{
        						if (abs(qbf.matrix[nclauses[j-pclauses.size()]][k])==var+1) {continue;}
        						if (qbf.qlevel[var]<qbf.qlevel[abs(qbf.matrix[nclauses[j-pclauses.size()]][k])-1]) {outofscope=true;break;}
        						
        						// FIXME: this is cheating!!!
        						//if (qbf.prefix[var]<qbf.prefix[abs(qbf.matrix[nclauses[j-pclauses.size()]][k])-1]) {outofscope=true;break;}
        						
        						if (depon_vars.find(abs(qbf.matrix[nclauses[j-pclauses.size()]][k]))!=depon_vars.end()) {continue;}
								depon_vars.insert(std::pair<int,int>(abs(qbf.matrix[nclauses[j-pclauses.size()]][k]),0));				
        						}
        					if (outofscope) {break;}
        					}
        				}
        			}
        		if (outofscope) 
        			{
        			if (qbf.verb>0) {printf("OUT_OF_SCOPE\n");} 
        			//continue;
        			break;
        			}
    			ngood++;
        		if (qbf.verb>0)
        			{
        			printf("[total: %d/%lu]", ndefcls, pclauses.size()+nclauses.size());
        			printf("\n");
        			}
        		
        		gate.varname = var+1;
        		gate.selfindex = gates.size();
        		gate.pos_cls = posdefcls;
        		gate.neg_cls = negdefcls;
        		gate.dep_vars.clear();

        		for (map<int,int>::iterator it=depon_vars.begin(); it!=depon_vars.end(); ++it) {gate.dep_vars.push_back(it->first);}
        		gates.push_back(gate);
        		}
			break;
        	}
       	} 
	} 
//================================================================================================= 
#endif