#ifndef Minisat_Matrix_h
#define Minisat_Matrix_h

#include <vector>
#include <set>
#include <algorithm>

using std::vector;
using std::set;
using std::sort;
namespace Minisat {
   
static int ndigits (int number) 
	{
	int answer=0;
	if (number==0) {return 1;}
	while (number>0) {number/=10;answer++;}
	return answer;
	}

struct Matrix
	{
	int nvars;                   // Number of variables
	int nuniv;                   // Number of univ vars
	int nexist;                  // Number of exist vars
	int nclauses;                // Number of clauses
    bool isqbf;                  // Is the problem QBF?
    vector<int> prefix; 	     // QBF prefix; also will be used as activation of variables (also shows quantification index)
    vector<int> qlevel;          // Quantification level of each variable
    vector<int> clauselevel;     // maximum quantification level of a clause
    vector<int> clauseindex;     // maximum quantification index of a clause
    vector<vector<int> > matrix; // 2dim array containing CNF
    vector<vector<int> > poccs;  // 2dim array containing positive occurrences
    vector<vector<int> > noccs;  // 2dim array containing negative occurrences 
    
    set<int> units;              // unit queu;
    vector<int> esize;           // effective clause sizes for UP (-1 means satisfied, 0 means falsified)
    vector<int> assign;          // assignments of unit variables 
    int verb;                    // verbosity level
    bool manyco;                 // if to generate COs for each CNF clause
    bool negateMatrix;           // if to negate the matrix output
    };    
    
struct Gate
	{
	int selfindex;               // index of this gate inside of the gate array
	int varname;                 // name of the defined variable (always positive)
	bool sign;                   // sign of the defined variable
	int type;                    // 1: AND; 2: XOR; 3: Semantic
	vector<int> dep_vars;        // set of variables on which defined variable depends (negative if it is negative in definition)
	vector<int> pos_cls;         // list of clauses within definition where defined variable is in positive phase
	vector<int> neg_cls;         // -//- negative phase
	bool status;                 // whether gate was chosen or not while circuit building (true is chosen)
	int varindex;                // prefix index of varname;
	int maxdepindex;             // maximal index of variable in dep_vars
    };    

//=================================================================================================
// Sorting int vector according to weights from another vector
struct intpair {int value; int weight;};
struct myveclt {bool operator() (intpair a, intpair b) {return a.weight < b.weight;}};
struct abslt   {bool operator() (int a, int b) {return abs(a) < abs(b);}};

struct matrixlt
	{
	const vector<vector<int> > *matrix;
	
	bool operator() (int a, int b)
		{
		if ((*matrix)[a].size() < (*matrix)[b].size()) {return true;}
		else if ((*matrix)[a].size() > (*matrix)[b].size()) {return false;}
		else 
			{
			//assume that clauses are sorted here
			for (unsigned i=0;i<(*matrix)[a].size();i++)
				{
				if (abs( (*matrix)[a][i] ) < abs( (*matrix)[b][i] )) {return true;}
				if (abs( (*matrix)[a][i] ) > abs( (*matrix)[b][i] )) {return false;}
				}
			}
		return a<b;
		}
	};

void WeightedSort(vector<int>& array, const vector<int>& weights) 
	{
	vector<int> copyarray;
	vector<intpair> order;
	intpair pair1;
	myveclt lessthan;

	for (unsigned i=0; i<array.size(); i++) {pair1.value=i;pair1.weight=weights[i];order.push_back(pair1);}
	sort(order.begin(),order.end(),lessthan);

	for (unsigned i=0;i<order.size();i++) {copyarray.push_back(array[order[i].value]);}
	array = copyarray;

	return;
	}		

void AbsSort(vector<int>& array) 
	{
	abslt lessthan;
	sort(array.begin(),array.end(),lessthan);
	return;
	}
//=================================================================================================

void BuildOccLists(Matrix& qbf)
	{
	vector<int> dummy;
	qbf.poccs.clear();
	qbf.noccs.clear();

	for (int i=0;i<qbf.nvars;i++) {qbf.poccs.push_back(dummy);qbf.noccs.push_back(dummy);}
	for (unsigned i=0;i<qbf.matrix.size();i++)
		{
		for (unsigned j=0; j<qbf.matrix[i].size() ; j++ )
			{
			int var = abs(qbf.matrix[i][j]);
			if (qbf.matrix[i][j]>0) {qbf.poccs[var-1].push_back(i);}
			else {qbf.noccs[var-1].push_back(i);}
			}
		}
	}
	
//=================================================================================================
//FIXME : need to optimize this; as well as building occurrence lists	
int UnitPropagation(Matrix& qbf, int& nunits)
	{
	int nclauses = qbf.matrix.size();  
	int nerases = 0;
	for (int i=0;i<nclauses;i++) {qbf.esize.push_back(qbf.matrix[i].size());}
	for (int i=0;i<qbf.nvars;i++) {qbf.assign.push_back(0);}
	
	while (qbf.units.size()>0)
		{
		nunits++;
		int unit = *(qbf.units.begin());
		qbf.units.erase(qbf.units.begin());
		
		if (qbf.assign[abs(unit)-1] == (unit > 0 ? -1 : 1)) {return -1;} //opposite phase units;
		qbf.assign[abs(unit)-1] = (unit > 0 ? 1 : -1);
		
		vector<int> *occs, *noccs;
		
		occs  = &(unit>0 ? qbf.poccs[abs(unit)-1] : qbf.noccs[abs(unit)-1]);
		noccs = &(unit<0 ? qbf.poccs[abs(unit)-1] : qbf.noccs[abs(unit)-1]);
		
		for (unsigned i=0;i<occs->size();i++) 
			{
			if (qbf.esize[(*occs)[i]]==-1) {continue;}
			qbf.esize[(*occs)[i]]=-1;
			nclauses--;
			nerases++;
			if (nclauses==0) {return 1;} //no clauses left;
			}
		for (unsigned i=0;i<noccs->size();i++) 
			{
			if (qbf.esize[(*noccs)[i]]==-1) {continue;}
			qbf.esize[(*noccs)[i]]--;
			nerases++;
			if (qbf.esize[(*noccs)[i]]==0) {return -1;}
			if (qbf.esize[(*noccs)[i]]==1)
				{
				int newunit = 0;
				for (unsigned j=0;j<qbf.matrix[(*noccs)[i]].size();j++)
					{
					if (qbf.assign[abs(qbf.matrix[(*noccs)[i]][j])-1]==0) {newunit = qbf.matrix[(*noccs)[i]][j]; break;}
					}
				assert(newunit != 0);
				// NOTE: it could happen that unit was inserted twice 
				// assert(qbf.units.find(newunit)==qbf.units.end());
				qbf.units.insert(newunit);
				}
			}
		}

  // optimization to see if we need to reconstruct the whole formula or just erase some elements from it
  if (nerases > 1000)
  {	  
    vector<vector<int> > new_matrix;
	vector<int> dummy;

	for (unsigned i=0;i<qbf.matrix.size();i++)
		{
		dummy.clear();
		if (qbf.esize[i]==-1) 
			continue;

		if (qbf.esize[i]==(int)qbf.matrix[i].size())
			{
			new_matrix.push_back(qbf.matrix[i]);
			continue;
			}
		for (unsigned j=0;j<qbf.matrix[i].size();j++)
			{
			if (qbf.assign[abs(qbf.matrix[i][j])-1]==0) 
				dummy.push_back(qbf.matrix[i][j]);
			}
		assert(dummy.size()>1);
		new_matrix.push_back(dummy);
		}
	qbf.matrix = new_matrix;
	new_matrix.clear();
  }	
  else 
  {	  
	unsigned i = 0;
	while (i < qbf.matrix.size())
		{
		if (qbf.esize[i]==-1) 
			{
			qbf.matrix.erase(qbf.matrix.begin()+i);
			qbf.esize.erase(qbf.esize.begin()+i);
			continue;
			}
		unsigned j = 0;
		while (j < qbf.matrix[i].size())
			{
			if (qbf.assign[abs(qbf.matrix[i][j])-1]!=0) 
				{
				qbf.matrix[i].erase(qbf.matrix[i].begin()+j);
				continue;
				}
			j++;
			}
		assert(qbf.matrix[i].size()!=1);
		i++;
		}
  }

	qbf.nclauses = qbf.matrix.size();
	assert(qbf.nclauses == nclauses);
	
	if (nclauses==0) {return 1;}
	
	BuildOccLists(qbf);
	return 0;
	}	
//=================================================================================================

}
#endif
