#ifndef Minisat_Syntactic_h
#define Minisat_Syntactic_h

#include <stdio.h>
#include "mtl/Vec.h"
#include "core/Matrix.h"
#include <vector>
#include <algorithm>

using std::vector;
using std::sort;
using namespace Minisat;

//=================================================================================================
//assumption: vecs are sorted increasingly
bool VecIncludes(const vector<int>& big, const vector<int>& sorted_indexes, const vector<int>& small, vector<int>& index_subset) 
	{
	int index = 0;
	bool found;
	
	index_subset.clear();
	
	for (unsigned i=0;i<small.size();i++)
		{
		found = false;
		for (unsigned j=index;j<sorted_indexes.size();j++)
			{
			if (small[i] == big[sorted_indexes[j]]) 
				{
				found=true;
				index_subset.push_back(sorted_indexes[j]);
				index = j+1;
				break;
				}
			}
		if (!found) {return false;}
		}
	return true;
	}	
//=================================================================================================
//sorting clauses according to their size, and literals	
void ClauseSort(const vector<vector<int> >& matrix, vector<int>& sorted_clause_order)
	{
	assert(sorted_clause_order.size() == matrix.size());
	
	matrixlt lessthan;
	
	lessthan.matrix = &matrix;
	
	sort(sorted_clause_order.begin(),sorted_clause_order.end(),lessthan);
	}
//=================================================================================================
//check if two sorted clauses are variable equivalent
bool AbsEqual(const vector<vector<int> >& matrix, int first, int second)
	{
	if (matrix[first].size()!=matrix[second].size()) {return false;}
	for (unsigned i=0;i<matrix[first].size();i++)
		{
		if (abs(matrix[first][i]) != abs(matrix[second][i])) {return false;}
		}
	return true;
	}	
//=================================================================================================
//check if given gate is a real XOR gate
int CheckXor(const vector<vector<bool> >& seq)
	{
	int height = seq.size();
	if (height==0) {return 0;}
	int width = seq[0].size();
	
	if (2*height!=pow(2,width)) {return 0;}
	
	if (width==1) {return seq[0][0] ? 1 : -1;}
	
	vector<vector<bool> > local0,local1;
	vector<bool> dummy;
	
	for (int i=0;i<height;i++)
		{
		dummy.clear();
		for (int j=1;j<width;j++) {dummy.push_back(seq[i][j]);}
		if (seq[i][0]) {local0.push_back(dummy);}
		else {local1.push_back(dummy);}
		} 
	int answer0 = CheckXor(local0);
	if (answer0==0) {return 0;}
	
	int answer1 = CheckXor(local1);
	if (answer1==0) {return 0;}
	
	if (answer0==answer1) {return 0;}
	
	return answer0;
	}
//=================================================================================================		
int Realxor(const vector<vector<int> >& matrix, const vector<int>& xorgate)	
	{
	if (xorgate.size()<2) {return 0;}
	
	int vars = matrix[xorgate[0]].size();
	if (2*xorgate.size()!=pow(2,vars)) {return 0;}
	
	vector<int> varsize;
	vector<vector<bool> > seq;
	vector<bool> dummy;
	
	for (int i=0;i<vars;i++) {varsize.push_back(0);}
	for (unsigned i=0;i<xorgate.size();i++)
		{
		dummy.clear();
		for (int j=0;j<vars;j++)
			{
			varsize[j] += ( matrix[xorgate[i]][j] > 0 ? 1 : -1 );
			dummy.push_back(matrix[xorgate[i]][j] > 0);
			}
		seq.push_back(dummy);
		}
	for (int i=0;i<vars;i++) {if (varsize[i]!=0) {return 0;}}
	return (CheckXor(seq));
	}
//=================================================================================================
/*

AND gate:   -5 4 3 2 -1 0
this means:  5 = -4 + -3 + -2 + 1

AND gate:    4 -3 2 -1 0
this means:  4 = -3 * 2 * -1

AND gate:    6 0 (-6 0)
this means:  6 = constant 1 (0)

*/
void FindAndGates (Matrix& qbf, vector<Gate>& gates)
	{
	Gate gate;
	gate.type = 1;
	gate.status = false;
	
    vector<int> binarylits, binarymap;
    vector<int> subclause, subset_indexes, clause_indexes;
    
    vector<int> order;       
    for (int i=0;i<qbf.nvars;i++) {order.push_back(i);} 
    WeightedSort(order,qbf.prefix);
        
    //looking for AND-like syntactic definitions
    for (int i=0 ; i < 2*qbf.nvars ; i++)
        {
        int var = order[i/2]; 
        bool sign = (i%2!=0); //false means positive phase
        if (qbf.prefix[var]<=0) {continue;} // universal variables
        	
        vector<vector<int> >* occlist  = (!sign ? &(qbf.poccs) : &(qbf.noccs));
        vector<vector<int> >* nocclist = (sign  ? &(qbf.poccs) : &(qbf.noccs));
        
        //look for positive definitions
        binarylits.clear();
        clause_indexes.clear();
        
        gate.dep_vars.clear();
        gate.pos_cls.clear();
        gate.neg_cls.clear();

        for (unsigned j=0;j<(*occlist)[var].size();j++)
        	{
        	int index = (*occlist)[var][j];
        	// UNIT clause
        	// we do explicit UP so it is impossible to have units now 
        	assert(qbf.matrix[index].size()!=1);
        	if (qbf.matrix[index].size()==1)
        		{
        		if (qbf.verb>0)
    				{
    				printf(" - %d : AND-GOOD! [total: 1/%lu]\n", var+1, qbf.poccs[var].size() + qbf.noccs[var].size());
       				}
       			gate.varname = var+1;
       			gate.sign = sign;
       			gate.selfindex = gates.size();
       			if (sign) {gate.pos_cls.push_back(index);}
       			else      {gate.neg_cls.push_back(index);}
       			gates.push_back(gate);
       			break;
       			}
       		if (qbf.matrix[index].size()!=2) {continue;}
       		//NOTE: pushing negation of a literal for subset check
       		binarylits.push_back( abs(qbf.matrix[index][0])==(var+1) ? -qbf.matrix[index][1] : -qbf.matrix[index][0] );
			clause_indexes.push_back(index);
       		}
       	
       	// NOTE: we do not look for definitions a=b here, as they will be later found by XOR gates 
       	if (binarylits.size()>1) 
       		{
       		binarymap.clear();
       		for (unsigned j=0;j<binarylits.size();j++) {binarymap.push_back(j);}
       		
       		WeightedSort(binarymap,binarylits);

       		for (unsigned j=0;j<(*nocclist)[var].size();j++)
       			{
       			int index = (*nocclist)[var][j];
       			subclause.clear();
       			
       			// do not consider variables with greater quantification level
       			if (qbf.clauselevel[index]>qbf.qlevel[var]) {continue;}
       			
       			// FIXME: this is cheating!!!!
       			//if (qbf.clauseindex[index]>qbf.prefix[var]) {continue;}
       			
       			// do not consider binary clauses (they will be found by XOR)
       			if (qbf.matrix[index].size()==2) {continue;}
       			
       			for (unsigned k=0;k<qbf.matrix[index].size();k++) 
       				{
       				if (abs(qbf.matrix[index][k])!=var+1) {subclause.push_back(qbf.matrix[index][k]);}
       				else 
       					{
       					assert((qbf.matrix[index][k] > 0) == sign);
       					}
       				}
       			sort(subclause.begin(),subclause.end());
       		
        		if (VecIncludes(binarylits,binarymap,subclause,subset_indexes))
        			{
        			if (qbf.verb>0)
        				{
        				printf(" - %d : AND-GOOD! [total: %lu/%lu]\n", var+1, subclause.size(), qbf.poccs[var].size() + qbf.noccs[var].size());
        				}
        			// adding the definition  
        			gate.varname = var+1;
       				gate.sign = sign;
       				gate.selfindex = gates.size();
       				
       				for (unsigned k=0;k<subclause.size();k++) {gate.dep_vars.push_back(subclause[k]);}
       				
        			// marking the clauses participating in the definition	
       				if (sign) 
       					{
       					for (unsigned k=0;k<subset_indexes.size();k++) {gate.pos_cls.push_back(clause_indexes[subset_indexes[k]]);}
       					gate.neg_cls.push_back(index);
       					}
       				else 
       					{
       					for (unsigned k=0;k<subset_indexes.size();k++) {gate.neg_cls.push_back(clause_indexes[subset_indexes[k]]);}
       					gate.pos_cls.push_back(index);
       					}

       				gates.push_back(gate);
        			break;
        			}
        		}
        	}
        }
	}
	
//=================================================================================================
void FindXorGates (Matrix& qbf, const vector<bool>& active_clauses, vector<Gate>& gates)
	{
	Gate gate;
	gate.type = 2;
	gate.status = false;
	
	//sorting the clauses by size and literals to look for XOR constraints
    vector<int> sorted_clause_order;
    for (int i=0;i<qbf.nclauses;i++) {sorted_clause_order.push_back(i);}
    ClauseSort(qbf.matrix,sorted_clause_order);
        
    //looking for XOR-like syntactic definitions
    int first_clause = 0;
    int next_clause = 1;
    vector<int> xorgate;
        
    xorgate.push_back(sorted_clause_order[first_clause]);
        
    bool last_round = false;    
    while (true)
        {
        // NOTE: if clauses participate in an AND definition we do not consider them for XOR definitions
        if (!last_round && !active_clauses[sorted_clause_order[next_clause]]) 
        	{
        	next_clause++;
        	if (next_clause >= qbf.nclauses) {last_round = true;}
        	continue;
        	}
        	
        // find all the clauses equal modulo variables phases
        if (!last_round && AbsEqual(qbf.matrix,sorted_clause_order[first_clause],sorted_clause_order[next_clause]))
        	{
        	xorgate.push_back(sorted_clause_order[next_clause]);
        	next_clause++;
        	if (next_clause >= qbf.nclauses) {last_round = true;}
        	}
        else 
        	{
        	// check if the clause is real XOR gate or not
        	int answer = Realxor(qbf.matrix,xorgate); // 0 if not; +- 1 depending on the sign of the gate
        	if (answer!=0)
        		{
				for (unsigned i=0;i<qbf.matrix[xorgate[0]].size();i++)
					{
					int var = abs(qbf.matrix[xorgate[0]][i])-1;

					// definition is our of order
					if (qbf.clauselevel[xorgate[0]]>qbf.qlevel[var])
						{
						if (qbf.verb>0) printf(" - %d : XOR OUT OF ORDER\n", var+1);
						continue;
						}
					// FIXME: this is cheating!!!
					/*if (qbf.clauseindex[xorgate[0]]>qbf.prefix[var])
						{
						if (qbf.verb>0) printf(" - %d : XOR OUT OF ORDER\n", var+1);
						continue;
						}*/

					if (qbf.verb>0) printf(" - %d : XOR-GOOD! [total: %lu/%lu]\n", var+1, xorgate.size(), qbf.poccs[var].size() + qbf.noccs[var].size());
        				
        			gate.varname = var+1;
       				gate.sign = (answer==1);
       				gate.selfindex = gates.size();
       				gate.dep_vars.clear();
       				
       				for (unsigned j=0;j<qbf.matrix[xorgate[0]].size();j++)
        				{
        				if (i!=j) {gate.dep_vars.push_back(qbf.matrix[xorgate[0]][j]);}
        				}
        				
        			// mark the clauses of the gate
        			// FIXME: need to distinguish positive and negative here!!!
        			gate.pos_cls = xorgate;	
        			gates.push_back(gate);
					}
				}	
			if (last_round) {break;}
			
        	first_clause = next_clause;
        	xorgate.clear();
        	xorgate.push_back(sorted_clause_order[first_clause]);
        	next_clause++;	
        	if (next_clause >= qbf.nclauses) {last_round = true;}
        }	}
    }
//=================================================================================================	
#endif