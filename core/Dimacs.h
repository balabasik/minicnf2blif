/****************************************************************************************[Dimacs.h]
Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson
Copyright (c) 2007-2010, Niklas Sorensson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************************************/

#ifndef Minisat_Dimacs_h
#define Minisat_Dimacs_h

#include <stdio.h>
#include "utils/ParseUtils.h"
#include "mtl/Sort.h"
#include "core/Matrix.h"
#include <math.h>
#include <vector>

using std::vector;
namespace Minisat {

//=================================================================================================
// DIMACS Parser:

template<class B>
static void readClause(B& in, vector<int>& lits) 
	{
    int parsed_lit;
    for (;;)
    	{
        parsed_lit = parseInt(in);
        if (parsed_lit == 0) break;
        lits.push_back(parsed_lit);
    	}
	}
	
//=================================================================================================

template<class B>
static void parse_DIMACS_main(B& in, Matrix& qbf) 
	{
    vector<int> lits;
    int cnt     = 0;
    int level   = 0;

    qbf.isqbf = true;
    
    for (;;)
    	{
        skipWhitespace(in);
        if (*in == EOF) break;
        else if (*in == 'p')
        	{
            if (eagerMatch(in, "p cnf"))
            	{
                qbf.nvars    = parseInt(in);
                qbf.nclauses = parseInt(in);
                for (int i=0;i<qbf.nvars;i++)    
                	{
                	qbf.prefix.push_back(0);
                	qbf.qlevel.push_back(0);
                	}
                for (int i=0;i<qbf.nclauses;i++) 
                	{
                	qbf.clauselevel.push_back(0);
                	qbf.clauseindex.push_back(0);
                	}
                }
        	else {printf("PARSE ERROR! Unexpected char: %c\n", *in), exit(3);}
        	} 
        else if (*in == 'c' || *in == 'p') {skipLine(in);}
        else if (*in == 'e')
        	{
        	++in;
        	level++;
        	lits.clear();
        	readClause(in, lits);
        	qbf.nexist = lits.size();
        	for (int i=0;i<lits.size();i++) 
        		{
        		cnt++;
        		qbf.prefix[lits[i]-1]=cnt;
        		qbf.qlevel[lits[i]-1]=level;
        		}
        	}
        else if (*in == 'a')
        	{
        	++in;
        	level++;
        	lits.clear();
        	readClause(in, lits);
        	qbf.nuniv = lits.size();
        	for (int i=0;i<lits.size();i++) 
        		{
        		cnt++;
        		qbf.prefix[lits[i]-1]=-cnt;
        		qbf.qlevel[lits[i]-1]=level;
        		}
        	}
        else
        	{
            //readClause(in, matrix);
            lits.clear();
            readClause(in, lits);
            
            AbsSort(lits);
			
			int lit_idx = 0;
			bool nextiter = false;
			while (lit_idx + 1 < lits.size())
				{
				if (lits[lit_idx] == lits[lit_idx+1]) {lits.erase(lits.begin()+lit_idx);continue;}
				if (lits[lit_idx] == -lits[lit_idx+1]) {nextiter = true; break;}
				lit_idx++;
				}
			if (nextiter) continue;
			
			if (lits.size()==1) {qbf.units.insert(lits[0]);continue;}
            
            for (int i=0;i<lits.size();i++) 
            	{
            	if (qbf.qlevel[abs(lits[i])-1] > qbf.clauselevel[qbf.matrix.size()]) 
            		{qbf.clauselevel[qbf.matrix.size()] = qbf.qlevel[abs(lits[i])-1];}
            	if (abs(qbf.prefix[abs(lits[i])-1]) > qbf.clauseindex[qbf.matrix.size()]) 
            		{qbf.clauseindex[qbf.matrix.size()] = abs(qbf.prefix[abs(lits[i])-1]);}
            	}
            	
            qbf.matrix.push_back(lits);
            }
    	}

    //assert(qbf.nclauses == (qbf.matrix.size()+qbf.units.size()));	
    	
    if (cnt == 0)
	 	{
	 	qbf.isqbf = false;
    	for (int i=0;i<qbf.nvars;i++) {qbf.prefix[i]=i+1;}
    	}
    BuildOccLists(qbf);
	}

//=================================================================================================

static void parse_DIMACS(gzFile input_stream, Matrix& qbf) {
    StreamBuffer in(input_stream);
    parse_DIMACS_main(in, qbf); }
    
//=================================================================================================
}
#endif
