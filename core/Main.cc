/*****************************************************************************************[Main.cc]
Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson
Copyright (c) 2007-2010, Niklas Sorensson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************************************/

#include <errno.h>

#include <signal.h>
#include <zlib.h>

#include "utils/System.h"
#include "utils/ParseUtils.h"
#include "utils/Options.h"
#include "core/Dimacs.h"
#include "core/Syntactic.h"
#include "core/Semantic.h"
#include "core/Circuit.h"

using std::vector;
using namespace Minisat;

//=================================================================================================

int main(int argc, char** argv) {
try 
    {
    setUsageHelp("USAGE: %s [options] <input-file> <result-output-file>\n\n  where input may be either in plain or gzipped DIMACS.\n");
   
	/*
	#if defined(__linux__)
       	fpu_control_t oldcw, newcw;
       	_FPU_GETCW(oldcw); newcw = (oldcw & ~_FPU_EXTENDED) | _FPU_DOUBLE; _FPU_SETCW(newcw);
       	printf("WARNING: for repeatability, setting FPU to use double precision\n");
	#endif
	*/		
	
	//------------------------------------------------------------------------------------
    // Extra options: 
    
    IntOption     verb        ("MAIN", "verb",   "Verbosity level (0=silent, 1=some, 2=more).", 0, IntRange(0, 2));
    IntOption     cpu_lim     ("MAIN", "cpu-lim","Limit on CPU time allowed in seconds.\n", INT32_MAX, IntRange(0, INT32_MAX));
    IntOption     mem_lim     ("MAIN", "mem-lim","Limit on memory usage in megabytes.\n", INT32_MAX, IntRange(0, INT32_MAX));
    BoolOption    manyco      ("MAIN", "manyco", "Generate CO for each clause not present in definition", false);
    BoolOption    negate      ("MAIN", "negate", "Negates the output (should be true for using with ABC)", false);
	StringOption  toblif      ("MAIN", "toblif", "If specified, resulting circuit is written to BLIF");
	
	//------------------------------------------------------------------------------------
		
    parseOptions(argc, argv, true);
    double initial_time = cpuTime();
        
    printf("\n================================================\n");
    printf(" STARTING the program.\n");
        
    //------------------------------------------------------------------------------------
        /*
        // Set limit on CPU-time:
        if (cpu_lim != INT32_MAX){
            rlimit rl;
            getrlimit(RLIMIT_CPU, &rl);
            if (rl.rlim_max == RLIM_INFINITY || (rlim_t)cpu_lim < rl.rlim_max){
              	rl.rlim_cur = cpu_lim;
               	if (setrlimit(RLIMIT_CPU, &rl) == -1)
                	printf("WARNING! Could not set resource limit: CPU-time.\n");
        	} }

        // Set limit on virtual memory:
        if (mem_lim != INT32_MAX){
           	rlim_t new_mem_lim = (rlim_t)mem_lim * 1024*1024;
        	rlimit rl;
        	getrlimit(RLIMIT_AS, &rl);
        	if (rl.rlim_max == RLIM_INFINITY || new_mem_lim < rl.rlim_max){
              	rl.rlim_cur = new_mem_lim;
               	if (setrlimit(RLIMIT_AS, &rl) == -1)
                   	printf("WARNING! Could not set resource limit: Virtual memory.\n");
           	} }
        */
    //------------------------------------------------------------------------------------

    if (argc == 1) {printf("Reading from standard input... Use '--help' for help.\n");}
    gzFile in = (argc == 1) ? gzdopen(0, "rb") : gzopen(argv[1], "rb");
    if (in == NULL) {printf("ERROR! Could not open file: %s\n", argc == 1 ? "<stdin>" : argv[1]), exit(1);}
        
    Matrix qbf;
    
    qbf.verb         = verb;
    qbf.manyco       = manyco;
    qbf.negateMatrix = negate;
                
    parse_DIMACS(in, qbf);
    gzclose(in);
    
    vector<bool> active_clauses;
    for (int i=0;i<qbf.nclauses;i++) {active_clauses.push_back(true);}
       
    double parsetime = cpuTime(); 
    printf(" Parsing done. [ #V: %d = #A: %d + #E: %d | #C: %d ] Time: %f\n", qbf.nvars, qbf.nuniv, qbf.nexist, qbf.nclauses, parsetime-initial_time);
    
    int nunits = 0;
    int unitanswer = UnitPropagation(qbf,nunits);
    if (unitanswer!=0) 
	  {
	  printf(" %s ! Solved by unit propagation!\n", (unitanswer==1 ? "TRUE" : "FALSE"));
	  printf("================================================\n\n");
	  return 0;
	  }
    
    double unittime = cpuTime(); 
    printf(" Unit propagation done. Units: %d Clauses: %lu Time: %f\n", nunits, qbf.matrix.size(),unittime-parsetime);
    
    //------------------------------------------------------------------------------------
    vector<Gate> gates;
    //vector<vector<int> > var_gate_idx;
    vector<int>  syntactic, emptyvec;
    
    for (int i=0;i<qbf.nvars;i++) {syntactic.push_back(0);}//var_gate_idx.push_back(emptyvec);}

	FindAndGates(qbf, gates);
	int nAndGates = gates.size();
	
    for (unsigned i=0;i<gates.size();i++) 
    	{
    	assert(gates[i].type == 1);
    	syntactic[gates[i].varname-1]++; 
    	//var_gate_idx[gates[i].varname-1].push_back(i);
    	for (unsigned j=0;j<gates[i].pos_cls.size();j++) active_clauses[gates[i].pos_cls[j]] = false;
    	for (unsigned j=0;j<gates[i].neg_cls.size();j++) active_clauses[gates[i].neg_cls[j]] = false;          
        }
        
    double andsyntime = cpuTime();
	printf(" Syntactic check done.  Found AND gates: %d Time: %f\n", nAndGates, andsyntime - unittime);

    //------------------------------------------------------------------------------------

    FindXorGates(qbf, active_clauses, gates);
    
    int nXorGates = gates.size() - nAndGates;
       
    for (unsigned i=nAndGates;i<gates.size();i++) 
    	{
    	assert(gates[i].type == 2);
    	syntactic[gates[i].varname-1]++;
    	//var_gate_idx[gates[i].varname-1].push_back(i);
		for (unsigned j=0;j<gates[i].pos_cls.size();j++) active_clauses[gates[i].pos_cls[j]] = false;
    	for (unsigned j=0;j<gates[i].neg_cls.size();j++) active_clauses[gates[i].neg_cls[j]] = false;          
		}
        
 	double syntime = cpuTime();	
    printf("                        Found XOR gates: %d Time: %f\n", nXorGates, syntime - andsyntime);    
    
    //------------------------------------------------------------------------------------
    	
    FindSemanticGates(qbf, syntactic, active_clauses, gates);
        
    int nSemGates = gates.size() - nAndGates - nXorGates;
    
    /*for (unsigned i=nAndGates+nXorGates;i<gates.size();i++) 
    	{
    	assert(gates[i].type == 3);
    	var_gate_idx[gates[i].varname-1].push_back(i);        
		}*/
    
    double semtime = cpuTime();
    printf(" Semantic  check done.  Found SEM gates: %d Time: %f\n", nSemGates, semtime-syntime);
        
    //------------------------------------------------------------------------------------
   
   	int npis=0, nfresh=0, ngates=0;
   
    BuildCircuit(toblif, qbf, gates, npis, nfresh, ngates); 
    
    double cycletime = cpuTime();
    printf(" Building the circuit done. [ #PI: %d | #FPI: %d | #Gates: %d ] Time: %f\n", npis, nfresh, ngates, cycletime-semtime);
    
    //------------------------------------------------------------------------------------

    printf(" Finished. Execution time: %f\n", cpuTime()-initial_time);
    printf("================================================\n\n");              
	return 0;	
    } 
    	
	//------------------------------------------------------------------------------------
   
catch (OutOfMemoryException&)
    {
    printf("===============================================================================\n");
    printf("INDETERMINATE\n");
    exit(0);
    }
}

//=================================================================================================

