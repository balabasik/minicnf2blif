#ifndef Minisat_Circuit_h
#define Minisat_Circuit_h

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include <assert.h>

using namespace std;

//========================================================================================
//========================================================================================
struct myintpair {int index; int weight;};

struct pairLT {
  bool operator() (myintpair a,myintpair b) { return (a.weight<b.weight);}
};

//========================================================================================
//========================================================================================
void writeGexf(string filename, const vector<vector<int> >& vgraph) {

stringstream buffer;
int nvars = vgraph.size();
//-----------------------------
buffer << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << endl;
buffer << "<gexf xmlns=\"http://www.gexf.net/1.2draft\" ";
buffer << "xmlns:viz=\"http://www.gexf.net/1.1draft/viz\" ";
buffer << "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ";
buffer << "xsi:schemaLocation=\"http://www.gexf.net/1.2draft http://www.gexf.net/1.2draft/gexf.xsd\" ";
buffer << "version=\"1.2\">" << endl;

buffer << "\t<meta>" << endl;
buffer << "\t\t<creator>Valera</creator>" << endl;
buffer << "\t\t<description>A variable dependency graph</description>" << endl;
buffer << "\t</meta>" << endl;

buffer << "\t<graph defaultedgetype=\"directed\">" << endl;

buffer << "\t\t<attributes class=\"node\">" << endl;
buffer << "\t\t\t<attribute id=\"0\" title=\"type\" type=\"string\"/>" << endl;
//buffer << "\t\t\t<attribute id=\"1\" title=\"color\" type=\"boolean\"/>" << endl;
buffer << "\t\t</attributes>" << endl;

buffer << "\t\t<nodes>" << endl;
for (int i=0;i<nvars;i++)
	{
	buffer << "\t\t\t<node id=\"" << i << "\" label=\"var" << i << "\">" << endl;
	buffer << "\t\t\t\t<viz:color r=\"239\" g=\"173\" b=\"66\" a=\"0.6\"/>" << endl;
    buffer << "\t\t\t\t<attvalues>" << endl;
    buffer << "\t\t\t\t\t<attvalue for=\"0\" value=\"var\"/>" << endl;
    buffer << "\t\t\t\t</attvalues>" << endl;
    buffer << "\t\t\t</node>" << endl;
    }
    
buffer << "\t\t</nodes>" << endl;
buffer << "\t\t<edges>" << endl;
int counter=0;
    
for (int i=0;i<nvars;i++)
	{
	for (unsigned j=0;j<vgraph[i].size();j++)
		{
		int source = vgraph[i][j];
		int target = i;
      	buffer << "\t\t\t<edge id=\"" << counter << "\" source=\"" << source;
      	buffer << "\" target=\"" << target << "\"/>" << endl;
      	counter++;
      	}
    }

buffer << "\t\t</edges>" << endl;
buffer << "\t</graph>" << endl;
buffer << "</gexf>" << endl;
//-----------------------------
filename = filename + ".gexf";
ofstream gexf_file;
gexf_file.open(filename.c_str());
gexf_file << buffer.str();
gexf_file.close();
return;
}

//========================================================================================
//========================================================================================

void BuildCircuit(const char * toblif, Matrix& qbf, vector<Gate>& gates, int& npis, int& nfresh, int &ngates) { 

vector<vector<int> > deps;
vector<vector<int> > back_deps; //note always picks backward dependencies of the first node in deps
vector<int> status, dummy, back_deps_counter, deps_counter; //deps_counter counts only 1st definition
int deps_local;
vector<int> dummy2;
map<int,int> mymap;
map<int,int>::iterator it;
int counter = 0;
int varname1, varname2;
vector<int> unit_vars;
vector<int> backmap;
vector<int> pis;

int cvars = 0, idefs = 0, odefs = 0, cdefs = 0, units = 0, inputs = 0, outputs = 0, removed_definitions = 0;

//----------------------------------------------------------------------------------------
	for (unsigned j=0;j<gates.size();j++)
		{
		bool was_already_defined = false;
		it = mymap.find(gates[j].varname);
		if (it!=mymap.end()) {varname1 = it->second;}
		else 
			{
			counter++;
			varname1 = counter;
			mymap.insert(std::pair<int,int>(gates[j].varname,counter));
			backmap.push_back(gates[j].varname);
			deps.push_back(dummy2);
			deps_counter.push_back(0);
			back_deps.push_back(dummy);
			back_deps_counter.push_back(0);
			status.push_back(-1);
			}
		if (status[varname1-1] == 0) {was_already_defined = true;}
	
		status[varname1-1] = 0;   //found to be a definition of something
	
		//deps_local.clear();
		bool just_found_unit = false;
		
		// found unit, assign it for sure
		// NOTE: this is no longer possible as we did unit propagation
		assert(gates[j].dep_vars.size()!=0);
		if (gates[j].dep_vars.size()==0)
			{
			unit_vars.push_back(varname1-1);
			just_found_unit = true;
			}
	
		deps_local = j;
	
		for (unsigned k=0;k<gates[j].dep_vars.size();k++)
			{
			int var = abs(gates[j].dep_vars[k]);
			
			it = mymap.find(var);
			if (it!=mymap.end()) {varname2 = it->second;}
			else 
				{
				counter++;
				varname2 = counter;
				mymap.insert(std::pair<int,int>(var,counter));
				backmap.push_back(var);
								
				deps.push_back(dummy2);
				deps_counter.push_back(0);
				back_deps.push_back(dummy);
				back_deps_counter.push_back(0);
				status.push_back(-1);
				}
			//deps_local.push_back(varname2-1);
			if (!was_already_defined) 
				{
				back_deps[varname2-1].push_back(varname1-1);
				back_deps_counter[varname2-1]++;
				}
			}
		if (just_found_unit) {continue;}
		if (deps[varname1-1].size()==0) {deps_counter[varname1-1] = gates[deps_local].dep_vars.size();}// = deps_local.size();}
		deps[varname1-1].push_back(deps_local);
		}
		
/*for (map<int,int>::iterator it = mymap.begin();it!=mymap.end();++it)
	{
	printf("map: %d %d\n",it->first,mymap[it->first]);
	}*/
/*for (int i=0;i<deps.size();i++) 
	{
	if (deps[i].size()!=0) printf("%d %d\n",backmap[i],deps[i][0]);
	}*/
//----------------------------------------------------------------------------------------
vector<int> touched_vars;
int current_var;

/*
status.
0: not checked yet
1: inner gate
2: unit
3: input (unconstrained)
4: output (unconstrained) 
5: broken (cyclic) definition (i.e. cinput)
6: chosen as a cyclic definition (i.e. coutput)
*/	

// NOTE: since we did unit propagation
assert(unit_vars.size()==0);	
for (unsigned i=0;i<unit_vars.size();i++)
	{
	current_var = unit_vars[i];
	if (status[current_var]>1) {continue;}
	status[current_var] = 2; 
	units++;
	if (deps[current_var].size()==0) {continue;}
	for (unsigned j=0;j<gates[deps[current_var][0]].dep_vars.size();j++) 
		{
		int vv = mymap[abs(gates[deps[current_var][0]].dep_vars[j])]-1;
		if (back_deps_counter[vv]>0) {back_deps_counter[vv]--;}
		//if (back_deps_counter[deps[current_var][0][j]]==0 && status[deps[current_var][0][j]]<1) {touched_vars.push_back(deps[current_var][0][j]);} //might go a few times through the same variable
		}
	}	
	
//cout << " Parsing input finished.   [ initial vars           : " << counter << " ]" << endl;
	
// propagate definitions from "PIs" (unconstrained inputs)

for (int i=0;i<counter;i++) 
	{
	//cout << backmap[i] << " " << deps_counter[i] << endl;
	if (status[i]<1 && deps_counter[i]==0) 
		{
		touched_vars.push_back(i);
		status[i]=3;
		inputs++;
		//cout << backmap[i] << endl;
		}
	}	

while (touched_vars.size()>0)
	{
	current_var = touched_vars.back();
	//cout << "current: " << backmap[current_var] << endl;
	touched_vars.pop_back();
	assert(status[current_var]!=1);
	//if (status[current_var]==1 || deps_counter[current_var]>0) {continue;}
	if (status[current_var]<1) 
		{
		status[current_var] = 1; 
		gates[deps[current_var][0]].status = true;
		//cout << "curr: " << current_var << " gate: " << deps[current_var][0] << " var: " << gates[deps[current_var][0]].varname << endl;
		idefs++;
		//cout << backmap[current_var] << endl;
		}
	for (unsigned j=0;j<back_deps[current_var].size();j++) 
		{
		deps_counter[back_deps[current_var][j]]--;
		if (deps_counter[back_deps[current_var][j]]==0 && status[back_deps[current_var][j]]<1) {touched_vars.push_back(back_deps[current_var][j]);} //might go a few times through the same variable
		}	
	}
	
//cout << " Propagating PIs finished. [ inputs / gates         : " << inputs << " / " << idefs+units << " ]" << endl;
// propagate definitions backwards from "POs" (unconstrained outputs)	

touched_vars.clear();
for (int i=0;i<counter;i++) 
	{
	//cout << backmap[i] << " " << back_deps_counter[i] << endl;
	if (status[i]<1 && back_deps_counter[i]==0) 
		{
		touched_vars.push_back(i);
		status[i] = 4;
		gates[deps[i][0]].status = true; 
		outputs++;
		}
	}

while (touched_vars.size()>0)
	{
	current_var = touched_vars.back();
	touched_vars.pop_back();
	assert(status[current_var]!=1);
	//if (status[current_var]==1 || back_deps_counter[current_var]>0) {continue;}
	if (status[current_var]<1) 
		{
		status[current_var] = 1;
		gates[deps[current_var][0]].status = true; 
		//cout << "curr: " << current_var << " gate: " << deps[current_var][0] << " var: " << gates[deps[current_var][0]].varname << endl;
		odefs++;
		}
	for (unsigned j=0;j<gates[deps[current_var][0]].dep_vars.size();j++) 
		{
		int vv = mymap[abs(gates[deps[current_var][0]].dep_vars[j])]-1;
		back_deps_counter[vv]--;
		if (back_deps_counter[vv]==0 && status[vv]<1) {touched_vars.push_back(vv);} //might go a few times through the same variable
		}
	}

//cout << " Propagating POs finished. [ outputs / gates        : " << outputs << " / " << odefs << " ]" << endl;	

/*int remainder_counter = 0;
vector<vector<int> > remainder;
map<int,int> remainder_map;
map<int,int>::iterator remainder_it;
		
it = mymap.begin();		
while (it!=mymap.end())
	{
	//cout << it->first << " " << status[it->second -1] << endl;
	if (status[it->second-1]==0) 
		{
		remainder_it = remainder_map.find(it->second);
		if (remainder_it!=remainder_map.end()) {varname1 = remainder_it->second;}
		else 
			{
			remainder_counter++;
			varname1 = remainder_counter;
			remainder_map.insert(std::pair<int,int>(it->second,remainder_counter));
			remainder.push_back(dummy);
			}
		for (int i=0;i<deps[it->second-1].size();i++)
			{
			if (status[deps[it->second-1][i]]==0) 
				{
				remainder_it = remainder_map.find(deps[it->second-1][i]+1);
				if (remainder_it!=remainder_map.end()) {varname2 = remainder_it->second;}
				else 
					{
					remainder_counter++;
					varname2 = remainder_counter;
					remainder_map.insert(std::pair<int,int>(deps[it->second-1][i]+1,remainder_counter));
					remainder.push_back(dummy);
					}
				remainder[varname1-1].push_back(varname2-1);
				}
			}
		}
	++it;
	}*/		
		
/*cout << "remainder: " << remainder.size() << " / " << counter << endl;
for (int i=0;i<remainder.size();i++)
	{
	cout << i+1 << " : ";
	for (int j=0;j<remainder[i].size();j++)
		{
		cout << remainder[i][j]+1 << " ";
		}
	cout << endl;
	}*/
	
touched_vars.clear();
vector<bool> visited;

for (int i=0;i<counter;i++) 
	{
	visited.push_back(false);
	if (status[i]==0) 
		{
		touched_vars.push_back(i);
		cvars++;
		//cout << backmap[i] << endl;
		}
	}
		
/*for (int i=0;i<counter;i++) 
	{
	if (status[i]==0) 
		{
		bool good = false;
		for (int j=0;j<deps[i].size();j++)
			{
			for (int k=0;k<deps[i][j].size();k++)
				{
				if (status[deps[i][j][k]]!=0) {good = true;break;}
				}
			if (good) {break;}
			}
		if (good)
			{
			touched_vars.push_back(i);
			cvars++;
			//cout << backmap[i] << endl;
			}
		}
	}	*/

//sorting according to definition sizes
vector<myintpair> vecpair;
myintpair mypair;
for (unsigned i=0;i<touched_vars.size();i++)
	{
	mypair.index = touched_vars[i];
	mypair.weight = deps[touched_vars[i]].size();
	//mypair.weight = -backmap[i]; 
	vecpair.push_back(mypair);
	}
pairLT lessthan;
sort(vecpair.begin(),vecpair.end(),lessthan);
touched_vars.clear();
for (unsigned i=0;i<vecpair.size();i++) {touched_vars.push_back(vecpair[vecpair.size()-i-1].index);}

//for (int current_index = 0; current_index < touched_vars.size(); current_index++)
while (touched_vars.size()>0)
	{
	current_var = touched_vars.back();
	touched_vars.pop_back();
	
	//current_var = touched_vars[current_index];
	//assert(status[current_var]==0);
	
	//check if the chosen definition creates cycles
	bool cyclic = false;
	//bool priority = false;
	int depindex=0;
	
	if (status[current_var]>0) {continue;}
	//cout << backmap[current_var] << endl;

	//cyclic check
	/*for (int i=0;i<deps[current_var].size();i++)
		{
		cyclic=false;
		//priority = false;
		for (int j=0;j<deps[current_var][i].size();j++)
			{
			if (visited[deps[current_var][i][j]]) {cyclic = true;break;}
			//if (status[deps[current_var][i][j]]!=0) {priority=true;}
			}
		if (!cyclic) 
			{
			depindex=i;
			//if (priority) {break;}
			break;
			}
		}*/

	if (cyclic) 
		{
		printf("cyclic!!\n");
		//cin >> number;
		//assert(status[current_var]==0);
		status[current_var]=5;
		removed_definitions++;
		continue;
		}
	else 
		{
		//assert(status[current_var]==0);
		if (status[current_var]==0)
			{
			//coutputs++;
			//status[current_var]=6;
			status[current_var]=5;
			removed_definitions++;
			}
		else 
			{
			status[current_var]=1;
			gates[deps[current_var][depindex]].status = true;
			//cout << "curr: " << current_var << " gate: " << deps[current_var][depindex] << " var: " << gates[deps[current_var][depindex]].varname << endl;
			cdefs++;
			}
		
		visited[current_var]=true;
		
		for (unsigned i=0;i<gates[deps[current_var][depindex]].dep_vars.size();i++) 
			{
			int vv = mymap[abs(gates[deps[current_var][depindex]].dep_vars[i])]-1;
			if (depindex==0) 
				{
				if (back_deps_counter[vv]>0) {back_deps_counter[vv]--;}
				if (back_deps_counter[vv]==0 && status[vv]==0) 
					{
					status[vv]=-1; // to indicate that this is under revision
					touched_vars.push_back(vv);
					}
				}
			/*if (status[deps[current_var][depindex][i]]==0) 
				{
				touched_vars.push_back(deps[current_var][depindex][i]);
				status[deps[current_var][depindex][i]]=-1; // to indicate that this is under revision
				}*/
			}
		
		for (unsigned i=0;i<back_deps[current_var].size();i++) 
			{
			if (deps_counter[back_deps[current_var][i]]>0) {deps_counter[back_deps[current_var][i]]--;}
			if (deps_counter[back_deps[current_var][i]]==0 && status[back_deps[current_var][i]]==0) 
				{
				status[back_deps[current_var][i]]=-1; // to indicate that this is under revision
				touched_vars.push_back(back_deps[current_var][i]);
				}
			}
		}	
	}

vector<bool> active_clauses, active_vars;
vector<int> fresh_vars;
for (int i=0;i<qbf.nclauses;i++) {active_clauses.push_back(true);}
for (int i=0;i<qbf.nvars;i++) {active_vars.push_back(qbf.assign[i]==0);}

for (unsigned i=0;i<gates.size();i++)
	{
	if (!gates[i].status) {continue;}
	ngates++;
	for (unsigned j=0;j<gates[i].pos_cls.size();j++) {active_clauses[gates[i].pos_cls[j]]=false;}
	//NOTE: as we use positive clauses for semantic gates we have to keep negative clauses
	if (gates[i].type==3) {continue;}
	for (unsigned j=0;j<gates[i].neg_cls.size();j++) {active_clauses[gates[i].neg_cls[j]]=false;}
	}
for (unsigned i=0;i<active_clauses.size();i++) 
	{
	if (!active_clauses[i]) {continue;}
	fresh_vars.push_back(i);
	}

assert(ngates == outputs + odefs + idefs + cdefs);

for (int i=0;i<qbf.nvars;i++)
	{
	if (qbf.prefix[i]<0) {pis.push_back(i);}
	}

for (unsigned i=0;i<status.size();i++)
	{
	if (status[i]!=1 && status[i]!=4) {continue;}
	active_vars[backmap[i]-1] = false;
	}

for (int i=0;i<qbf.nvars;i++)
	{
	if (!active_vars[i]) {continue;}
	if (qbf.prefix[i]<0) {continue;}
	//cout << "pi: " << i << endl;
	pis.push_back(i);
	}
	
npis = pis.size();
nfresh = fresh_vars.size();

if (!toblif) {return;}

stringstream buffer;

buffer << "# universal " << qbf.nuniv << endl;
buffer << ".model qbf" << endl;
buffer << ".inputs";

for (unsigned i=0;i<pis.size();i++) {buffer << " n" << pis[i]+1;}
buffer << endl << ".outputs out";

if (!qbf.manyco) {buffer << endl;}
else 
	{
	for (unsigned i=0;i<fresh_vars.size();i++)
		{
		//check if forall part is not empty
		bool u = false;
		for (unsigned j=0;j<qbf.matrix[fresh_vars[i]].size();j++) 
			{
			if (qbf.prefix[abs(qbf.matrix[fresh_vars[i]][j])-1]<0) {u=true;break;}
			}
		if (u) {buffer << " a" << i;}
		}
	buffer << endl;
	}

//NOTE: firstly writing units
for (unsigned i=0;i<qbf.assign.size();i++)
	{
	if (qbf.assign[i]==0) {continue;}
	buffer << ".names n" << i+1 << endl;
	buffer << (qbf.assign[i]==1 ? "1" : "0") << endl;
	}

for (unsigned i=0;i<gates.size();i++)
	{
	if (!gates[i].status) {continue;}

	if (gates[i].type==1 || gates[i].type==2)
		{
		buffer << ".names ";
		for (unsigned j=0;j<gates[i].dep_vars.size();j++) {buffer << "n" << abs(gates[i].dep_vars[j]) << " ";}
		buffer << "n" << gates[i].varname << endl;
		}
	// NOTE: AND gate
	if (gates[i].type==1)
		{
		for (unsigned j=0;j<gates[i].dep_vars.size();j++) {buffer << (gates[i].dep_vars[j]>0 ? "0" : "1");}
		buffer << " " << (gates[i].sign ? "1" : "0") << endl; 
		}
	// NOTE: XOR gate
	else if (gates[i].type==2)
		{
		int xorsize = gates[i].dep_vars.size()-1;
		vector<bool> prev;
		bool lastbit;
		for (int j=0;j<xorsize;j++) {prev.push_back(false);}
		for (int j=0;j<pow(2,xorsize);j++)
			{
			for (unsigned k=0;k<prev.size();k++)
				{
				prev[k]=!prev[k];
				if (prev[k]) {break;}
				}
			
			lastbit = false;
			for (unsigned k=0;k<prev.size();k++)
				{
				buffer << (prev[k] ? "1" : "0");
				lastbit ^= prev[k];
				}
			buffer << (lastbit ? "1" : "0");	
			buffer << " " << (gates[i].sign ? "1" : "0") << endl;
			}
		}
	// NOTE: Semantic gate
	else if (gates[i].type==3)
		{
		map<int,int> inclausemap;
		//printf("varname: %d\n",gates[i].varname);
		for (unsigned j=0;j<gates[i].dep_vars.size();j++) {inclausemap.insert(std::make_pair<int,int>(abs(gates[i].dep_vars[j]),j));}
		//printf("%d ",gates[i].dep_vars[j]);printf("\n");
		
		assert(gates[i].pos_cls.size()>0 || gates[i].neg_cls.size()>0);
		
		if (gates[i].pos_cls.size()==0) 
			{
			buffer << ".names n" << gates[i].varname << endl << "0"<< endl;
			continue;
			}
		else if (gates[i].neg_cls.size()==0) 
			{
			buffer << ".names n" << gates[i].varname << endl << "1"<< endl;
			continue;
			}
		
		buffer << ".names ";
		for (unsigned j=0;j<gates[i].dep_vars.size();j++) {buffer << "n" << abs(gates[i].dep_vars[j]) << " ";}
		buffer << "n" << gates[i].varname << endl;
		
		for (unsigned j=0;j<gates[i].pos_cls.size();j++)
			{
			vector<int> var_signs;
			for (unsigned k=0;k<gates[i].dep_vars.size();k++) {var_signs.push_back(0);}
			for (unsigned k=0;k<qbf.matrix[gates[i].pos_cls[j]].size();k++)
				{
				int var = qbf.matrix[gates[i].pos_cls[j]][k];
				if (abs(var)==gates[i].varname) {continue;}
				//printf("var: %d\n", var);
				map<int,int>::iterator it = inclausemap.find(abs(var));
				assert(it!=inclausemap.end());
				var_signs[it->second] = (var>0 ? -1 : 1);
				}
			for (unsigned k=0;k<gates[i].dep_vars.size();k++) {buffer << (var_signs[k]==0 ? "-" : var_signs[k]==1 ? "1" : "0");}
			buffer << " 1" << endl;
			}
		}
	}
// OLD way
if (!qbf.manyco)
	{
	for (unsigned i=0;i<fresh_vars.size();i++)
		{
		buffer << ".names ";
		for (unsigned j=0;j<qbf.matrix[fresh_vars[i]].size();j++) {buffer << "n" << abs(qbf.matrix[fresh_vars[i]][j]) << " ";}
		buffer << "g" << i << endl;
		for (unsigned j=0;j<qbf.matrix[fresh_vars[i]].size();j++) {buffer << (qbf.matrix[fresh_vars[i]][j]>0 ? "0" : "1");}
		buffer << " 0" << endl;
		}
	}
// NEW way (separate universal and exist vars)
else
	{
	for (unsigned i=0;i<fresh_vars.size();i++)
		{
		//forall
		buffer << ".names ";
		for (unsigned j=0;j<qbf.matrix[fresh_vars[i]].size();j++) 
			{
			if (qbf.prefix[abs(qbf.matrix[fresh_vars[i]][j])-1]>0) {continue;}
			buffer << "n" << abs(qbf.matrix[fresh_vars[i]][j]) << " ";
			}
		buffer << "a" << i << endl;
		for (unsigned j=0;j<qbf.matrix[fresh_vars[i]].size();j++) 
			{
			if (qbf.prefix[abs(qbf.matrix[fresh_vars[i]][j])-1]>0) {continue;}
			buffer << (qbf.matrix[fresh_vars[i]][j]>0 ? "0" : "1");
			}
		buffer << " 0" << endl;
	
		//exists
		buffer << ".names ";
		for (unsigned j=0;j<qbf.matrix[fresh_vars[i]].size();j++) 
			{
			if (qbf.prefix[abs(qbf.matrix[fresh_vars[i]][j])-1]<0) {continue;}
			buffer << "n" << abs(qbf.matrix[fresh_vars[i]][j]) << " ";
			}
		buffer << "e" << i << endl;
		for (unsigned j=0;j<qbf.matrix[fresh_vars[i]].size();j++) 
			{
			if (qbf.prefix[abs(qbf.matrix[fresh_vars[i]][j])-1]<0) {continue;}
			buffer << (qbf.matrix[fresh_vars[i]][j]>0 ? "0" : "1");
			}
		buffer << " 0" << endl;
	
		buffer << ".names a" << i << " e" << i << " g" << i << endl;
		buffer << "00 0\n";
		}
	}
	
buffer << ".names ";
for (unsigned i=0;i<fresh_vars.size();i++) {buffer << "g" << i << " ";}
buffer << "out" << endl;	
for (unsigned i=0;i<fresh_vars.size();i++) {buffer << "1";}

//NOTE: here we have a choice to complement QBF (1 - is original, 0 - negated)
if (qbf.negateMatrix)
	buffer << " 0" << endl << ".end";
else
	buffer << " 1" << endl << ".end";

ofstream outblif;
outblif.open(toblif);
outblif << buffer.str();
outblif.close();

/*vector<vector<int> > final;
for (int i=0;i<counter;i++) {final.push_back(dummy);}
for (int i=0;i<counter;i++)
	{
	if (status[i]==1) {final[i] = deps[i];}
	}*/
		
//writeGexf(filename,deps);
//writeGexf(filename,remainder);
//writeGexf(filename,final);

return;
}


//=================================================================================================
#endif
